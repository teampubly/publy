package com.example.publy;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.example.publy.chat_tab.Chat_Data;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class SplashActivity extends Activity implements OnClickListener {

	private String Post_URL = "";
	/** スレッドUI操作用ハンドラ */
	private Handler mHandler = new Handler();
	/** テキストオブジェクト */
	private Runnable updateText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);

		// Button bu=(Button)findViewById(R.id.test_chat);
		findViewById(R.id.test_chat).setOnClickListener(this);
		findViewById(R.id.test_checkin).setOnClickListener(this);
		findViewById(R.id.test_detail).setOnClickListener(this);
		findViewById(R.id.test_post).setOnClickListener(this);
		findViewById(R.id.test_setting).setOnClickListener(this);
		findViewById(R.id.test_gps).setOnClickListener(this);

		pref_check();
	}

	private void pref_check() {
		updateText = new Runnable() {
			public void run() {

				Intent intent = new Intent();
				intent.setClass(SplashActivity.this, Checkin_Activity.class);
				startActivity(intent);
			}
		};

		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = sharedPref.edit();
		String uuid = UUID.randomUUID().toString();
		editor.putString("chara_UUID", uuid);
		editor.commit();

		if (sharedPref.getBoolean("first_setting", false)) {
			first_setting();
		} else {

			mHandler.postDelayed(updateText, 3000);
		}

	}

	private void first_setting() {
		EditText edit_text = (EditText) findViewById(R.id.first_edit);
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		SpannableStringBuilder sb = (SpannableStringBuilder) edit_text
				.getText();
		String chat_text_data = sb.toString();
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost method = new HttpPost(Post_URL);

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_name", chat_text_data));
		params.add(new BasicNameValuePair("user_id", sharedPref.getString(
				"chara_UUID", sharedPref.getString("chara_UUID", ""))));

		try {
			method.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			HttpResponse response = client.execute(method);
			int status = response.getStatusLine().getStatusCode();
			// statusコードが２００だったらOK

			if (status == 200) {
				Editor editor = sharedPref.edit();
				editor.putBoolean("first_setting", true);
				editor.commit();
				mHandler.postDelayed(updateText, 100);

			} else {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						this);
				// アラートダイアログのタイトルを設定します
				alertDialogBuilder.setTitle("初期登録未完成");
				alertDialogBuilder.setCancelable(false);
				// アラートダイアログのメッセージを設定します
				alertDialogBuilder.setMessage("初期登録ができませんでした。再度起動してください。");
				// アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
				alertDialogBuilder.setPositiveButton("はい",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								finish();
							}
						});
				// アラートダイアログのキャンセルが可能かどうかを設定します
				alertDialogBuilder.setCancelable(true);
				AlertDialog alertDialog = alertDialogBuilder.create();
				// アラートダイアログを表示します
				alertDialog.show();
			}

		} catch (Exception e) {

		}
	}

	public void onClick(View v) {
		/* .... */
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.test_chat:
			intent.setClass(this, Chat_Activity.class);
			break;
		case R.id.test_checkin:
			intent.setClass(this, Checkin_Activity.class);
			break;
		case R.id.test_detail:
			intent.setClass(this, Detail_Activity.class);
			break;
		case R.id.test_post:
			intent.setClass(this, Post_Activity.class);
			break;
		case R.id.test_setting:
			intent.setClass(this, Setting_Activity.class);
			break;
		case R.id.test_gps:
			intent.setClass(this, GetGPSActivity.class);
			break;

		default:
			break;
		}

		startActivity(intent);
	}

	public class MyAsyncTask extends AsyncTask<String, Integer, Long> implements
			OnCancelListener {

		final String TAG = "MyAsyncTask";
		ProgressDialog dialog;
		Context context;

		public MyAsyncTask(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			dialog = new ProgressDialog(context);
			dialog.setTitle("取得中...");
			dialog.setMessage("ただいまデータを取得中です...");
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setCancelable(true);
			dialog.setOnCancelListener(this);

			dialog.show();
		}

		@Override
		protected Long doInBackground(String... params) {
			Log.d(TAG, "doInBackground - " + params[0]);

			try {
				HttpUriRequest httpGet = new HttpGet(Post_URL);
				DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
				HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
				HttpEntity entity = httpResponse.getEntity();
				Gson gson = new Gson();
				Log.v("aaa", EntityUtils.toString(entity));
				Type collectionType = new TypeToken<Collection<Chat_Data>>() {
				}.getType();
				// list_chat_data = gson.fromJson(EntityUtils.toString(entity),
				// collectionType);

				// Chat_Data chat_data =
				// gson.fromJson(EntityUtils.toString(entity),
				// Chat_Data.class);

			} catch (IOException e) {
				System.out.println("通信失敗");
			}
			return 123L;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			Log.d(TAG, "onProgressUpdate - " + values[0]);
			dialog.setProgress(values[0]);
		}

		@Override
		protected void onCancelled() {
			Log.d(TAG, "onCancelled");
			dialog.dismiss();
		}

		@Override
		protected void onPostExecute(Long result) {
			Log.d(TAG, "onPostExecute - " + result);
			dialog.dismiss();
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			Log.d(TAG, "Dialog onCancell... calling cancel(true)");
			this.cancel(true);
		}
	}

}
