package com.example.publy;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.publy.chat_tab.Chat_Data;
import com.example.publy.chat_tab.CustomAdapter;
import com.example.publy.chat_tab.Json_Get;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class Chat_Tab_Activity extends Activity implements OnClickListener {
	private static final String URL = "https://twitter.com/users/show/eiKatou.json";
	static final int ANIMATION_DURATION = 200;
	static final int REFRESH_DURATION = 3000;
	private static List<Chat_Data> list_chat_data = new ArrayList<Chat_Data>();
	private CustomAdapter customAdapater;
	private PullToRefreshListView listView;
	private Handler mMainHandler = new Handler();

	private EditText edit_text;
	private String Post_URL;
	private double lot = 0;
	private double lat = 0;

	@Override
	protected void onCreate(Bundle save) {
		// TODO Auto-generated method stub
		super.onCreate(save);

		Intent intent = getIntent();
		if (null != intent) {
			lot = intent.getDoubleExtra("lot", 0);
			lat = intent.getDoubleExtra("lat", 0);
		} else {
			lat = save.getDouble("lat");
			lot = save.getDouble("lot");
		}

		setContentView(R.layout.tab_activity_chat);
		edit_text = (EditText) findViewById(R.id.chat_edit);
		edit_text.setFocusable(true);
		edit_text.setFocusableInTouchMode(true);

		findViewById(R.id.chat_send_button).setOnClickListener(this);

		for (int i = 0; i < 10; i++) {
			Log.v("aaa", "test");
			list_chat_data.add(new Chat_Data(false, null, "test_chat_" + i,
					"11月1日", "青山一丁目"));
		}
		if (list_chat_data == null) {
			Log.v("aaa", "NULL");
		} else {
			Log.v("aaa", "NNNULL");
		}
		customAdapater = new CustomAdapter(this, 0, list_chat_data);

		listView = (PullToRefreshListView) findViewById(R.id.listview);
		// アダプターを設定します
		listView.setAdapter(customAdapater);
		// リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録します
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			}
		});
		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				Timer timer = new Timer(true);
				timer.schedule(new TimerTask() {
					@Override
					public void run() {

						mMainHandler.post(new Runnable() {
							public void run() {
								Json_Get json_get = new Json_Get();
								json_get.json_get(list_chat_data);
								// listView.onRefreshComplete();
								// for (Chat_Data chat_data :
								// list_chat_data) {
								// chat_data.chat_image_bitmap = null;
								// chat_data.chat_text_string = "pull";
								// }
								// customAdapater.notifyDataSetChanged();
							}
						});
					}
				}, REFRESH_DURATION);
			}
		});

		new MyAsyncTask(this).execute("Param1");

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState); /* ここで状態を保存 */
		Intent intent = getIntent();
		if (null != intent) {
			lot = intent.getDoubleExtra("lot", 0);
			lat = intent.getDoubleExtra("lat", 0);
			outState.putDouble("lot", lot);
			outState.putDouble("lat", lat);

		} else {

		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle save) {
		super.onRestoreInstanceState(save); /* ここで保存した状態を読み出して設定 */
		if (lot == 0) {

			lot = save.getDouble("lot");
		}

		if (lat == 0) {
			lat = save.getDouble("lat");
		}
	}

	public class MyAsyncTask extends AsyncTask<String, Integer, Long> implements
			OnCancelListener {

		final String TAG = "MyAsyncTask";
		ProgressDialog dialog;
		Context context;

		public MyAsyncTask(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			dialog = new ProgressDialog(context);
			dialog.setTitle("取得中...");
			dialog.setMessage("ただいまデータを取得中です...");
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setCancelable(true);
			dialog.setOnCancelListener(this);

			dialog.show();
		}

		@Override
		protected Long doInBackground(String... params) {
			Log.d(TAG, "doInBackground - " + params[0]);

			try {
				HttpUriRequest httpGet = new HttpGet(URL);
				DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
				HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
				HttpEntity entity = httpResponse.getEntity();
				Gson gson = new Gson();
				Log.v("aaa", EntityUtils.toString(entity));
				Type collectionType = new TypeToken<Collection<Chat_Data>>() {
				}.getType();
				// list_chat_data = gson.fromJson(EntityUtils.toString(entity),
				// collectionType);

				// Chat_Data chat_data =
				// gson.fromJson(EntityUtils.toString(entity),
				// Chat_Data.class);

			} catch (IOException e) {
				System.out.println("通信失敗");
			}
			return 123L;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			Log.d(TAG, "onProgressUpdate - " + values[0]);
			dialog.setProgress(values[0]);
		}

		@Override
		protected void onCancelled() {
			Log.d(TAG, "onCancelled");
			dialog.dismiss();
		}

		@Override
		protected void onPostExecute(Long result) {
			Log.d(TAG, "onPostExecute - " + result);
			dialog.dismiss();
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			Log.d(TAG, "Dialog onCancell... calling cancel(true)");
			this.cancel(true);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		add_Post();
	}

	private void add_Post() {

		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		SpannableStringBuilder sb = (SpannableStringBuilder) edit_text
				.getText();
		String chat_text_data = sb.toString();
		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost method = new HttpPost(Post_URL);

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("message", chat_text_data));
		params.add(new BasicNameValuePair("user_id", sharedPref.getString(
				"chara_UUID", "")));
		params.add(new BasicNameValuePair("lat", String.valueOf(lat)));
		params.add(new BasicNameValuePair("lot", String.valueOf(lot)));

		try {
			method.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			HttpResponse response = client.execute(method);
			int status = response.getStatusLine().getStatusCode();
			// statusコードが２００だったらOK

		} catch (Exception e) {

		}
	}
}