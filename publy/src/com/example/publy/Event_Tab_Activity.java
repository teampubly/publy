package com.example.publy;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.publy.chat_tab.Chat_Data;
import com.example.publy.chat_tab.CustomAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class Event_Tab_Activity extends Activity implements OnClickListener {

	static final int ANIMATION_DURATION = 200;
	static final int REFRESH_DURATION = 3000;
	private static List<Chat_Data> list_chat_data = new ArrayList<Chat_Data>();
	private CustomAdapter customAdapater;
	private PullToRefreshListView listView;
	private Handler mMainHandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.tab_activity_event);
		findViewById(R.id.event_send_button).setOnClickListener(this);

		for (int i = 0; i < 10; i++) {
			list_chat_data.add(new Chat_Data(false, null, "test_chat_" + i,
					"11月1日", "青山一丁目"));
		}

		customAdapater = new CustomAdapter(this, 0, list_chat_data);
		//
		//
		// ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1);
		// // アイテムを追加します
		// adapter.add("red");
		// adapter.add("green");
		// adapter.add("blue");

		listView = (PullToRefreshListView) findViewById(R.id.listview);
		// アダプターを設定します
		listView.setAdapter(customAdapater);
		// リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録します
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent();
				intent.setClass(Event_Tab_Activity.this, Post_Activity.class);
				startActivity(intent);
				// ListView listView = (ListView) parent;
				// // クリックされたアイテムを取得します
				// String item = (String) listView.getItemAtPosition(position);
				// Toast.makeText(Chat_Tab_Activity.this, item,
				// Toast.LENGTH_LONG).show();
			}
		});
		// リストビューのアイテムが選択された時に呼び出されるコールバックリスナーを登録します
		// listView.setOnItemSelectedListener(new
		// AdapterView.OnItemSelectedListener() {
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view,
		// int position, long id) {
		// // ListView listView = (ListView) parent;
		// // // 選択されたアイテムを取得します
		// // String item = (String) listView.getSelectedItem();
		// // Toast.makeText(Chat_Tab_Activity.this, item,
		// // Toast.LENGTH_LONG).show();
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> parent) {
		// }
		// });
		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				Timer timer = new Timer(true);
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						mMainHandler.post(new Runnable() {
							public void run() {

								listView.onRefreshComplete();
								for (Chat_Data chat_data : list_chat_data) {
									chat_data.chat_image_bitmap = null;
									chat_data.chat_text_string = "pull";
								}
								customAdapater.notifyDataSetChanged();
							}
						});
					}
				}, REFRESH_DURATION);
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setClass(Event_Tab_Activity.this, Post_Activity.class);
		startActivity(intent);

	}

}