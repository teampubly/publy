package com.example.publy.checkin;


public class Place_Data {
	public String latitude;
	public String longitude;
	public String place_name;

	public Place_Data(String latitude, String longitude, String place_name) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.place_name = place_name;
	}
}
