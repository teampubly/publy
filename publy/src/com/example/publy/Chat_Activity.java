package com.example.publy;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;

public class Chat_Activity extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_chat);
		initTabs();
	}

	protected void initTabs() {

		Resources res = getResources();
		TabHost tabHost = getTabHost();
		TabHost.TabSpec spec;
		Intent intent;

		// Tab1
		intent = new Intent().setClass(this, Chat_Tab_Activity.class);
		View view1 = View.inflate(getApplication(),
				R.layout.chat_top_tab_layout, null);
		// spec = tabHost
		// .newTabSpec("Tab1")
		// .setIndicator("チャット",
		// res.getDrawable(R.drawable.chat_tab_layout))
		// .setContent(intent);
		spec = tabHost.newTabSpec("Tab1").setIndicator(view1)
				.setContent(intent);
		tabHost.addTab(spec);

		// Tab2
		intent = new Intent().setClass(this, Event_Tab_Activity.class);
		View view2 = View.inflate(getApplication(),
				R.layout.event_top_tab_layout, null);
		spec = tabHost.newTabSpec("Tab2").setIndicator(view2)
				.setContent(intent);
		tabHost.addTab(spec);

		//

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_action_bar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.setting:
			Log.d("", "menu1 tap.");
			Intent intent =new Intent();
			intent.setClass(this, Setting_Activity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
