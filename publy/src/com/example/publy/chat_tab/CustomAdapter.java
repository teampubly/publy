package com.example.publy.chat_tab;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.publy.R;

public class CustomAdapter extends ArrayAdapter<Chat_Data> {
	private LayoutInflater layoutInflater_;

	public CustomAdapter(Context context, int textViewResourceId,
			List<Chat_Data> objects) {
		super(context, textViewResourceId, objects);
		layoutInflater_ = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 特定の行(position)のデータを得る
		Chat_Data item = (Chat_Data) getItem(position);

		// convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
		if (null == convertView) {
			if (item.isMine) {
				// 自分が送信したデータだったら
			} else {
				// 他人が送信したデータだったら
				convertView = layoutInflater_.inflate(
						R.layout.layout_chat_base, null);
			}
		}

		// CustomDataのデータをViewの各Widgetにセットする
		ImageView imageView;
		imageView = (ImageView) convertView.findViewById(R.id.chat_image);
		imageView.setImageResource(android.R.drawable.checkbox_on_background);
		// imageView.setImageBitmap(item.chat_image_bitmap);

		TextView textView;
		textView = (TextView) convertView.findViewById(R.id.chat_text);
		textView.setText(item.chat_text_string);

		return convertView;
	}
}
