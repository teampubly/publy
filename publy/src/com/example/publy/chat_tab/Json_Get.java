package com.example.publy.chat_tab;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Json_Get implements Runnable {
	private static final String URL = "https://twitter.com/users/show/eiKatou.json";
	List<Chat_Data> list_chat_data;

	public void json_get(List<Chat_Data> list_chat_data) {
		this.list_chat_data = list_chat_data;
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			HttpUriRequest httpGet = new HttpGet(URL);
			DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
			HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
			HttpEntity entity = httpResponse.getEntity();
			Gson gson = new Gson();
			Log.v("aaa", EntityUtils.toString(entity));
			Type collectionType = new TypeToken<Collection<Chat_Data>>() {
			}.getType();
			list_chat_data = gson.fromJson(EntityUtils.toString(entity),
					collectionType);
			// Chat_Data chat_data = gson.fromJson(EntityUtils.toString(entity),
			// Chat_Data.class);

		} catch (IOException e) {
			System.out.println("通信失敗");
		}
	}
	
	
	
}
