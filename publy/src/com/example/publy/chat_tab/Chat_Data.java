package com.example.publy.chat_tab;

import android.graphics.Bitmap;

public class Chat_Data {
	public boolean isMine;
	public Bitmap chat_image_bitmap;
	public String chat_text_string;
	public String time;
	public String where;

	public Chat_Data(boolean isMine, Bitmap img, String chat, String time,
			String where) {
		this.isMine = isMine;
		this.chat_image_bitmap = img;
		this.chat_text_string = chat;
		this.time = time;
		this.where = where;
	}
}
