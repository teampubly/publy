var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var UserSchema = new Schema({
	uuid: { type: String, unique: true, required: true }, 
	name: { type: String, required: true }
});

var PostSchema = new Schema({
	user_uuid: { type: String, required: true },
	place_id: { type: Number, required: true },
	latitude: { type: Number, required: true },
	longitude: { type: Number, required: true },
	message: { type: String, required: true },
	priority: { type: Number, required: true },
	created_at: { type: Date, required: true },
	deleted_at: { type: Date, required: true }
});

var PlaceSchema = new Schema({
	latitude: { type: Number, required: true },
	longitude: { type: Number, required: true },
	name: { type: String, required: true }
});

// mongoDB接続時のエラーハンドリング
// mongodb://[hostname]/[dbname]
mongoose.connect('mongodb://localhost/publy');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connecetion error'));
db.once('open', function() {
  console.log("Connected to 'Publy' database");
});

exports.User = mongoose.model('User', UserSchema);
exports.Post = mongoose.model('Post', PostSchema);
exports.Place = mongoose.model('Place', PlaceSchema);
