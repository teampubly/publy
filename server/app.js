
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var post = require('./routes/post');
var place = require('./routes/place')
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

/**
 *	本番で使うもの
 */
app.get('/users', user.index);
app.post('/users', user.create);

app.get('/posts', post.index);
app.get('/posts/show/:id', post.show);
app.post('/posts', post.create);

app.get('/places', place.index);
app.get('/places/show/:id', place.show);
app.post('/places', place.create);

/**
 *	debug用
 */
//app.get('users/show/:id', user.show);
//app.get('/users/new', user.new);
//app.get('/users/:id/edit', user.edit);
//app.post('/users/:id', user.update);
//app.post('/users/:id/destroy', user.destroy);

//app.get('/posts/new', post.new);
//app.get('/posts/:id/edit', post.edit);
//app.post('/posts/:id', post.update);
//app.post('/posts/:id/destroy', post.destroy);

app.get('/places/new', place.new);
//app.get('/places/:id/edit', place.edit);
//app.post('/places/:id', place.update);
//app.post('/places/:id/destroy', place.destroy);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
