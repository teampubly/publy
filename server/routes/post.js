/*
 * Post API
 * Post取得/作成
 */

var model = require('../model');
var Post = model.Post;

/** GET	/posts */
exports.index = function(req, res){
	var userLat = req.query.latitude;
	var userLot = req.query.longitude;

	// 数字に変換できないならBad Requestを返す
	if(isNaN(userLat) || isNaN(userLot))
		res.send(400);

	var margin = 0.0001;
	var now = Date.now();
	console.log(now);

	Post.find( { latitude: { $gt: userLat-margin, $lt: Number(userLat)+margin},
				longitude: { $gt: userLot-margin, $lt: Number(userLot)+margin},
				deleted_at: { $gt: now }
			}, function(err, docs){
  		if(err) throw err;

		res.send(docs);
	});
};

/** GET /posts:id */
exports.show = function(req, res){
	var id = req.params.id;

	Post.findOne( {_id: id}, function(err, docs){
		if(err) throw err;

		res.send(docs);
	});
};

/** /posts/new */
exports.new = function(req, res){
	res.render('post/index');
}

/** POST /posts */
exports.create = function(req, res){
	var userId = req.body.user_uuid;
	var placeId = req.body.place_id;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var message = req.body.message;
	var priority = req.body.priority;

	var createdAt = Date.now();
	var deleteAt = new Date(Date.now());
	if(priority==0)
		deleteAt.setMonth(deleteAt.getUTCMonth()+1);
	else
		deleteAt.setYear(deleteAt.getUTCFullYear()+10);
	// TODO 1回で取得する量を制限する
	var newPost = new Post({user_uuid: userId, place_id: placeId, latitude: latitude, longitude: longitude, message: message, priority: priority, created_at: createdAt, deleted_at: deleteAt});
	newPost.save(function(err){
		if(err) throw err;

		res.send(200);
	});
};


