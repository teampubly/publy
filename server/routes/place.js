/*
 * Place API
 * Place取得
 */

var model = require('../model');
var Place = model.Place;

/** GET	/places 
 *	
 */
exports.index = function(req, res){
  	Place.find(function(err, docs){
  		if(err) throw err;

		res.send(docs);
	});
};

/** GET /places:id */
exports.show = function(req, res){
	var id = req.params.id;
	console.log("id: " + id);

	Place.findOne( {_id: id}, function(err, docs){
		if(err) throw err;

		res.send(docs);
	});
};

/** /places/new */
exports.new = function(req, res){
	res.render('place/index');
}

/** POST /places */
exports.create = function(req, res){
	var placeName = req.body.name;
	var lat = req.body.latitude;
	var lot = req.body.longitude;

	var newPlace = new Place({name: placeName, latitude: lat, longitude: lot});
	newPlace.save(function(err){
		if(err) throw err;

		res.send(200);
	});
};

/** /places/:id/edit */
exports.edit = function(req, res){
	// デバッグ用に実装
}

/** POST /places/:id */
exports.update = function(req, res){
	// デバッグ用に実装
}

/** POST /places/:id/destroy */
exports.destroy = function(req, res){
	// デバッグ用に実装
}
