/*
 * User API
 * User取得/作成
 */

var model = require('../model');
var User = model.User;

/** GET	/users */
exports.index = function(req, res){
	User.find( {}, function(err, docs){	
		if(err){
			throw err;
		}

		res.send(docs);
	});
};

/** POST /users */
exports.create = function(req, res){
	var name = req.body.name;
	var id = req.body.id;

	var user = new User({name: name, uuid: id});
	user.save( function(err){
		if(err){
			throw err;
		}

		res.send(200);
	});
};




/*
 *	Debug API
 */


/** GET /users */
exports.show = function(req, res){
	var id = req.params.id;
	User.find( {_id: id}, function(err, docs){
		if(err){
			throw err;
		}

		res.send( docs );
	});
};


/** GET /users */
exports.new = function(req, res){
	res.render("./users/index.ejs");
};


/** GET /users/:id/edit */
exports.edit = function(req, res){
	res.send(200);
};


/** PATCH /users */
exports.update = function(req, res){
	res.send(200);
};


/* DELETE /users/:id/destroy */
exports.destroy = function(req, res){
	res.send(200);
};

